FROM python:3.7-stretch

WORKDIR /app
COPY . .
RUN pip install minicli
RUN pip install .[prod]
CMD ["gunicorn", "scuriolus:app", "--worker-class", "roll.worker.Worker", "--bind", "0.0.0.0:8080"]
EXPOSE 8080
